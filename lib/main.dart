import 'dart:developer';

import 'package:firebase_core/firebase_core.dart';
import 'package:flutter/material.dart';
import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:mymtnapp/addsession.dart';

void main() async {
  WidgetsFlutterBinding.ensureInitialized();
  await Firebase.initializeApp(
      options: FirebaseOptions(
  apiKey: "AIzaSyD-2CusKhnuXc1esp3motTHzv_vBH2Dlm8",
  authDomain: "mishaa-35f39.firebaseapp.com",
  projectId: "mishaa-35f39",
  storageBucket: "mishaa-35f39.appspot.com",
  messagingSenderId: "118430016052",
  appId: "1:118430016052:web:2ced3b712c198e91126061",
  measurementId: "G-ZHH0FR0P4K"));
  runApp(const MyApp());
}

class MyApp extends StatelessWidget {
  const MyApp({Key? key}) : super(key: key);

  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      debugShowCheckedModeBanner: false,
      title: 'Flutter Demo',
      theme: ThemeData(
        primarySwatch: Colors.pink,
      ),
      home: const MyHomePage(title: 'Group Study Sessions'),
    );
  }
}

class MyHomePage extends StatefulWidget {
  const MyHomePage({Key? key, required this.title}) : super(key: key);

  final String title;

  @override
  State<MyHomePage> createState() => _MyHomePageState();
}

class _MyHomePageState extends State<MyHomePage> {
  void _incrementCounter() {
    setState(() {
      FirebaseFirestore.instance
          .collection("notes")
          .add({"note title": "hi there"}).then((value) => log("successful"));
    });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text(widget.title),
      ),
      body: Center(
        child: Column(
            mainAxisAlignment: MainAxisAlignment.center,
            children: <Widget>[AddSession()]),
      ),
    );
  }
}
