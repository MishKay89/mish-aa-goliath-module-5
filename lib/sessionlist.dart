import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:flutter/material.dart';
import 'package:firebase_core/firebase_core.dart';
import 'package:flutter/widgets.dart';

class SessionList extends StatefulWidget {
  const SessionList({Key? key}) : super(key: key);

  @override
  State<SessionList> createState() => _SessionListState();
}

class _SessionListState extends State<SessionList> {
  final Stream<QuerySnapshot> _mySessions =
      FirebaseFirestore.instance.collection('sessions').snapshots();

  @override
  Widget build(BuildContext context) {
    TextEditingController _subjectFieldCtrl = TextEditingController();
    TextEditingController _locationFieldCtrl = TextEditingController();
    TextEditingController _timeFieldCtrl = TextEditingController();

    void _delete(docId) {
      FirebaseFirestore.instance
          .collection("sessions")
          .doc(docId)
          .delete()
          .then((value) => print('deleted'));
    }

    void _update(data) {
      var collection = FirebaseFirestore.instance.collection("sessions");
      _subjectFieldCtrl.text = data["Subject_Name"];
      _locationFieldCtrl.text = data["Location"];
      _timeFieldCtrl.text = data["Time"];

      showDialog(
          context: context,
          builder: (_) => AlertDialog(
                title: Text('Update'),
                content: Column(mainAxisSize: MainAxisSize.min, children: [
                  TextField(
                    controller: _subjectFieldCtrl,
                  ),
                  TextField(
                    controller: _locationFieldCtrl,
                  ),
                  TextField(
                    controller: _timeFieldCtrl,
                  ),
                  TextButton(
                    onPressed: () {
                      collection.doc(data["doc_id"]).update({
                        "Subject_Name": _subjectFieldCtrl.text,
                        "Location": _locationFieldCtrl.text,
                        "Time": _timeFieldCtrl.text,
                      });
                      Navigator.pop(context);
                    },
                    child: Text('Update'),
                  ),
                ]),
              ));
    }

    // TODO: implemet build

    return StreamBuilder(
      stream: _mySessions,
      builder: (BuildContext context,
          AsyncSnapshot<QuerySnapshot<Object?>> snapshot) {
        if (snapshot.hasError) {
          return Text('Something went wrong');
        }

        if (snapshot.connectionState == ConnectionState.waiting) {
          return CircularProgressIndicator();
        }

        if (snapshot.hasData) {
          return Row(
            children: [
              Expanded(
                child: SizedBox(
                  height: (MediaQuery.of(context).size.height),
                  width: MediaQuery.of(context).size.width,
                  child: ListView(
                    children: snapshot.data!.docs
                        .map((DocumentSnapshot documentSnapshot) {
                      Map<String, dynamic> data =
                          documentSnapshot.data()! as Map<String, dynamic>;
                      return Column(
                        children: [
                          Card(
                            child: Column(
                              children: [
                                ListTile(
                                  title: Text(data['Subject_Name']),
                                  subtitle: Text(
                                      data['Location'] + ' ' + data['Time']),
                                ),
                                ButtonTheme(
                                    child: ButtonBar(
                                  children: [
                                    OutlineButton.icon(
                                      onPressed: () {
                                        _update(data);
                                      },
                                      icon: Icon(Icons.edit),
                                      label: Text('Edit'),
                                    ),
                                    OutlineButton.icon(
                                      onPressed: () {
                                        _delete(data["doc_id"]);
                                      },
                                      icon: Icon(Icons.delete),
                                      label: Text('Delete'),
                                    ),
                                  ],
                                )),
                              ],
                            ),
                          ),
                        ],
                      );
                    }).toList(),
                  ),
                ),
              )
            ],
          );
        } else {
          return (Text('No data'));
        }
      },
    );
  }
}
