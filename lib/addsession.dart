import 'dart:developer';

import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:flutter/material.dart';

import 'sessionlist.dart';

class AddSession extends StatefulWidget {
  const AddSession({Key? key}) : super(key: key);

  @override
  State<AddSession> createState() => _AddSessionState();
}

class _AddSessionState extends State<AddSession> {
  @override
  Widget build(BuildContext context) {
    TextEditingController subjectController = TextEditingController();
    TextEditingController locationController = TextEditingController();
    TextEditingController timeController = TextEditingController();

    Future _addSessions() {
      final subject = subjectController.text;
      final location = locationController.text;
      final time = timeController.text;

      final ref = FirebaseFirestore.instance.collection("sessions").doc();

      return ref
          .set({
            "Subject_Name": subject,
            "Location": location,
            "Time": time,
            "doc_id": ref.id
          })
          .then((value) => log("Collection added!"))
          .catchError((onError) => log(onError));
    }

    return Column(
      children: [
        Column(
          children: [
            Padding(
              padding: EdgeInsets.symmetric(horizontal: 8, vertical: 8),
              child: TextField(
                controller: subjectController,
                decoration: InputDecoration(
                  border: OutlineInputBorder(
                    borderRadius: BorderRadius.circular(20),
                  ),
                  hintText: "Enter Subject Name",
                ),
              ),
            ),
            Padding(
              padding: EdgeInsets.symmetric(horizontal: 8, vertical: 8),
              child: TextField(
                controller: locationController,
                decoration: InputDecoration(
                  border: OutlineInputBorder(
                    borderRadius: BorderRadius.circular(20),
                  ),
                  hintText: "Enter Location",
                ),
              ),
            ),
            Padding(
              padding: EdgeInsets.symmetric(horizontal: 8, vertical: 8),
              child: TextField(
                controller: timeController,
                decoration: InputDecoration(
                  border: OutlineInputBorder(
                    borderRadius: BorderRadius.circular(20),
                  ),
                  hintText: "Enter Time",
                ),
              ),
            ),
            ElevatedButton(
                onPressed: () {
                  _addSessions();
                },
                child: Text("Add Session")),
          ],
        ),
        SessionList()
      ],
    );
  }
}
